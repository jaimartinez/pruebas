package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class TestCliente {
	
	static GestorContabilidad gestor;
	static Cliente cliente1;
	static Cliente cliente2;

	@BeforeAll
	public static void setUpBeforeClass() throws Exception{
		gestor = new GestorContabilidad();
		cliente1 = new Cliente("Jaime", "11111", LocalDate.now());
		cliente2 = new Cliente("Pepito", "22222", LocalDate.now());
	}
	
	@BeforeEach
	public void prepararTest() {
		gestor.listaClientes.clear();
	}
	
	// Buscar un cliente que exista
	@Test
	public void BuscarClienteExistente(){
		gestor.listaClientes.add(cliente1);
		
		Cliente actual = gestor.buscarCliente("11111");
		Cliente esperado = cliente1;
		
		assertSame(esperado, actual);
	}
	
	 //Buscar cliente que no exista
	@Test
	public void BuscarClienteNoExistente() {
		Cliente actual = gestor.buscarCliente("88888");
		assertNull(actual);
	}
	
	//Dar de alta un cliente que ya existe
	@Test
	public void AltaClienteExistente() {
		gestor.listaClientes.add(cliente1);
		gestor.altaCliente(cliente1);
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		
		assertEquals(esperado, actual);
	}
	
	//Dar de alta un cliente que no existe
	@Test
	public void AltaClienteNoExiste() {
		gestor.altaCliente(cliente1);
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		
		assertEquals(esperado, actual);
	}
	
	//Cliente mas antiguo de los varios que se encuentran en la lista
	@Test
	public void ClienteMasAntiguoExistente() {
		cliente1.setFechaAlta(LocalDate.now().minusYears(10));
		gestor.listaClientes.add(cliente1);
		gestor.listaClientes.add(cliente2);
		
		Cliente actual = gestor.clienteMasAntiguo();
		assertEquals(cliente1, actual);
	}
	
	//Cliente mas antiguo siendo que no hay ninguno en la lista
	@Test
	public void ClienteMasAntiguoNoExistente() {
		Cliente actual = gestor.clienteMasAntiguo();
		assertNull(actual);
	}
	
	//Eliminar cliente existente
	@Test
	public void EliminarClienteExistente() {
		gestor.listaClientes.add(cliente1);
		gestor.listaClientes.add(cliente2);
		gestor.eliminarCliente("11111");
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	
	//Eliminar cliente inexistente
	@Test
	public void EliminarClienteNoExistente() {
		gestor.listaClientes.add(cliente1);
		gestor.eliminarCliente("44444");
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	
	//Eliminar cliente siendo que hay ningun cliente en listaClientes
	@Test
	public void eliminarClienteExistente() {
		Cliente cliente = new Cliente();
		cliente.setDni("123");
		gestor.listaClientes.add(cliente);
		
		gestor.eliminarCliente("123");
		
		int actual = gestor.listaClientes.size();
		int esperado = 0;
		
		assertEquals(esperado, actual);
	}
	
	//Eliminar cliente siendo que no hay clientes en listaClientes
	@Test
	public void eliminarClienteInexistente() {
		String dni = "123";
		gestor.eliminarCliente(dni);
		
		int actual = gestor.listaClientes.size();
		int esperado = 0;
		
		assertEquals(esperado, actual);
	}

}
