package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestFactura {
	
	private static final LocalDate LocalDate = null;
	static GestorContabilidad gestor;
	
	@BeforeAll
	public static void prepararTest() {
		gestor = new GestorContabilidad();
	}
	
	@BeforeEach
	public void antesDeClase() {
		gestor.listaFacturas.clear();
	}

	//Buscar una factura que ya existe
	@Test
	public void buscarFacturaExistente() {
		Factura factura = new Factura();
		factura.setCodigoFactura("123");
		gestor.listaFacturas.add(factura);
		
		Factura actual = gestor.buscarFactura("123");
		Factura esperado = factura;
		
		assertEquals(esperado, actual);
	}
	
	//Buscar una factura que no existe
	@Test
	public void buscarFacturaInexistente() {
		String codigoFactura = "123";
		Factura actual = gestor.buscarFactura(codigoFactura);
		
		assertNull(actual);
	}
	
	//Crear factura que ya existe todavia
	@Test
	public void crearFacturaExistente() {
		String codigoFactura = "123";
		Factura factura = new Factura();
		gestor.crearFactura(factura);
		
		 int actual = gestor.listaFacturas.size();
		 int esperado = 1;
		  assertEquals(esperado, actual);
	}
	
	//Factura mas cara de dos que ya existen
	@Test
	public void facturaMasCaraDeDos() {
		float precioUnidad = 10.00F;
		Factura factura1 = new Factura();
		factura1.setPrecioUnidad(precioUnidad + 1.00F);
		gestor.listaFacturas.add(factura1);
		
		Factura factura2 = new Factura();
		gestor.listaFacturas.add(factura2);
		factura2.setPrecioUnidad(precioUnidad);
		
		Factura actual = gestor.facturaMasCara();
		Factura esperado = factura1;
		
		assertEquals(esperado, actual);
	}
	
	//Factura mas cara de tres que ya existen
	@Test
	public void facturaMasCaraDeTres() {
		float precioUnidad = 10.00F;
		
		Factura factura1 = new Factura();
		factura1.setPrecioUnidad(precioUnidad + 1.00F);
		gestor.listaFacturas.add(factura1);
		
		Factura factura2 = new Factura();
		factura2.setPrecioUnidad(precioUnidad);
		gestor.listaFacturas.add(factura2);
		
		Factura factura3 = new Factura();
		factura3.setPrecioUnidad(precioUnidad + 5.00F);
		gestor.listaFacturas.add(factura3);
		
		Factura actual = gestor.facturaMasCara();
		Factura esperado = factura3;
		
		assertEquals(esperado, actual);
	}
	
	//Facturacion anual de dos facturas con 10 euros
	@Test
	public void facturacionAnualDeFacturas() {
		float precioUnidad = 10.00F;
		
		Factura factura1 = new Factura();
		factura1.setPrecioUnidad(precioUnidad);
		factura1.setFecha(LocalDate.parse("2018-01-01"));
		gestor.listaFacturas.add(factura1);
		
		Factura factura2 = new Factura();
		factura2.setPrecioUnidad(precioUnidad);
		factura2.setFecha(LocalDate.parse("2018-01-01"));
		gestor.listaFacturas.add(factura2);
		
		float actual = gestor.calcularFacturacionAnual(2018);
		float esperado = 20.00F;
		
		assertEquals(esperado, actual);
	}
	
	//Facturacion anual si no hay ninguna factura en listaFacturas
	@Test
	public void facturacionAnualSinFacturas() {
		float actual = gestor.calcularFacturacionAnual(2018);
		float esperado = 0.00F;
		
		assertEquals(esperado, actual);
	}
	
	//Asignar factura a cliente
	@Test
	public void facturaACliente() {
		Cliente cliente1 = new Cliente();
		gestor.listaClientes.add(cliente1);
		
		Factura factura1 = new Factura();
		gestor.listaFacturas.add(factura1);
		
		gestor.asignarFacturaAcliente("123", "456");
		
		assertNull(factura1.getCliente());
	}
	
	//Cantidad de facturas por cliente, siendo que hay un cliente y una factura
	@Test
	public void facturasPorCliente() {
		Cliente cliente = new Cliente();
		gestor.listaClientes.add(cliente);
		
		Factura factura = new Factura();
		gestor.listaFacturas.add(factura);
		
		gestor.asignarFacturaAcliente("123", "456");
		
		int actual = gestor.cantidadFacturasporCliente("123");
		int esperado = 1;
		
		assertEquals(esperado, actual);
	}
	
	//Cantidad de facturas por cliente, siendo que hay un cliente sin ninguna factura
	@Test
	public void facturasPorClienteSinFactura() {
		Cliente cliente = new Cliente();
		gestor.listaClientes.add(cliente);
		
		int actual = gestor.cantidadFacturasporCliente("123");
		int esperado = 0;
		assertEquals(esperado, actual);
	}
	
	//Eliminar factura siendo que hay una factura en listaFacturas
	@Test
	public void eliminarFacturaExistente() {
		Factura factura = new Factura();
		factura.setCodigoFactura("123");
		gestor.listaFacturas.add(factura);
		
		gestor.eliminarFactura("123");
		
		int actual = gestor.listaFacturas.size();
		int esperado = 0;
		
		assertEquals(esperado, actual);
	}
	
	//Eliminar factura siendo que no hay facturas en listaFacturas
	@Test
	public void eliminarFacturaInexistente() {
		String codigoFactura = "123";
		gestor.eliminarFactura("123");
		
		int actual = gestor.listaFacturas.size();
		int esperado = 0;
		
		assertEquals(esperado,actual);
	}
	

}
